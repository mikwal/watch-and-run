#!/usr/bin/env node

let args = process.argv.slice(2);

if (args.length < 2) { 
    console.log("usage: watch-and-run <glob> <command> [args...]");
    process.exit(2);
}

let glob = args[0];
let command = args[1];

if (glob.includes(';')) {
    glob = glob.split(';');
}

require('./index.js')(glob, command, args.slice(2));