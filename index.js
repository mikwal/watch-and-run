let { spawn } = require('child_process');
let chokidar = require('chokidar');

module.exports = watchAndRun;


function watchAndRun(glob, command, args) {

    let isRunningCommand = false;
    let commandIsTriggered = false;

    let log = message => {
        console.log(`\x1B[1m\x1B[33m\x1B[40m*** ${message} ***\x1B[39m\x1B[22m\x1B[49m`);
    };

    let runCommand = () => {
        commandIsTriggered = false;
        isRunningCommand = true;
        
        let childProcess = spawn(command, args);
        
        childProcess.on('error', error => {
            log(`Error running command: ${error.errno}`);
        });

        childProcess.on('exit', (code, signal) => {
            
            code && log(`Command exited with code: ${code}`);
            signal && log(`Command exited because of signal: ${signal}`);
            isRunningCommand = false;
            if (commandIsTriggered) {
                runCommand();
            }
        });
        
        childProcess.stdout.pipe(process.stdout);
        childProcess.stderr.pipe(process.stderr);
    }

    chokidar.
        watch(glob, { ignoreInitial: true }).
        on('all', event => {
            switch (event) {
                case 'add':
                case 'change':
                case 'unlink':
                    commandIsTriggered = true;
                    if (!isRunningCommand) {
                        runCommand();
                    }
                    break;
            }
        });

}